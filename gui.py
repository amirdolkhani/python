from tkinter import *
import json
import io


class Contact:
    def __init__(self, firstname,lastname, phone, email, address):
        self.firstname = firstname
        self.lastname = lastname
        self.phone = phone
        self.email = email
        self.address = address

    def __repr__(self):
        """
        Create a nice representation of this object, for debug purposes
        """
        return json.dumps(self.__dict__)


class ContactManagerAPI:
    def __init__(self):
        self.contacts = self.read_contacts()

    def read_contacts(self):
        """
        Read contacts from contact json file
        """
        print('API: Reading contacts from disk')
        contacts = dict()
        try:
            with open('contacts.json') as contacts_json:
                 contacts = json.load(contacts_json)
        except Exception as e:
               print('No previous contacts file found')
        return contacts

    def write_contacts(self, contacts):
        """
        Write contacts to json filead
        """
        print('API: Writing contacts to disk')
        try:
            with io.open('contacts.json', 'w', encoding='utf8') as contacts_json_file:
                 data = json.dumps(contacts, default=self.json_converter, indent=4, ensure_ascii=False)
                 contacts_json_file.write(data)
        except Exception as e:
            print('Could not write to contact file due to {}'.format(e))

    def json_converter(self, obj):
        """
        Method for converting objects to JSON. Use normal toJSON method for known types.
        Use dictionary of object for unknown types.
        """
        try:
          return obj.toJSON()
        except:
          return obj.__dict__

    def add_contact(self,contact):
        print('API: Adding contact {0}'.format(contact))
        self.contacts[contact.firstname] = contact
        self.write_contacts(self.contacts)


class ContactManagerGUI:
    def __init__(self, api):
        self.api = api
        win = Tk()
        inputframe = Frame()
        inputframe.pack()

        firstnameentrylabel = Label(inputframe, text='First name').grid(row=0, column=0, sticky=W)
        self.firstname = StringVar()
        firstnameentry = Entry(inputframe, textvariable=self.firstname)
        firstnameentry.grid(row=0, column=1, sticky=W)

        lastnamelabel = Label(inputframe, text='Last name').grid(row=1, column=0, sticky=W)
        self.lastname = StringVar()
        lastnameentry = Entry(inputframe, textvariable=self.lastname)
        lastnameentry.grid(row=1, column=1, sticky=W)

        phonenumberlabel = Label(inputframe, text='Phone number').grid(row=2, column=0, sticky=W)
        self.phonenumber = StringVar()
        phonenumberentry = Entry(inputframe, textvariable=self.phonenumber)
        phonenumberentry.grid(row=2, column=1, sticky=W)

        emaillabel = Label(inputframe, text='E-mail').grid(row=3, column=0, sticky=W)
        self.email = StringVar()
        emailentry = Entry(inputframe, textvariable=self.email)
        emailentry.grid(row=3, column=1, sticky=W)

        addresslabel = Label(inputframe, text='Address').grid(row=4, column=0, sticky=W)
        self.address = StringVar()
        addressentry = Entry(inputframe, textvariable=self.address)
        addressentry.grid(row=4, column=1, sticky=W)

        buttonframe = Frame()
        buttonframe.pack()
        addbutton = Button(buttonframe, text='Add', command=self.add)
        addbutton.pack(side=LEFT)
        listbutton= Button(buttonframe,text="List",command=lambda:set_text(api.contacts))
        listbutton.pack(side=LEFT)
        addbutton = Button(buttonframe, text='Quit', command=self.die)
        addbutton.pack(side=LEFT)

        def set_text(list):
            contactlist.insert(0,list)
            return
        contactlist = Listbox(win, height=10,bg='yellow')
        contactlist.pack()

    def add(self):
        print('GUI: Adding contact {0},{1},{2},{3},{4}'.format(self.firstname.get(), self.lastname.get(), self.phonenumber.get(), self.email.get(), self.address.get()))
        contact = Contact(self.firstname.get(),self.lastname.get(),self.phonenumber.get(), self.email.get(), self.address.get())
        self.api.add_contact(contact)

    def die(self):
        exit()

    def run(self):
        mainloop()


api = ContactManagerAPI()
gui = ContactManagerGUI(api)
gui.run()
